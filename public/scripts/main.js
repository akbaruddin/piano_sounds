var keys = document.querySelectorAll(".key");
var note = document.querySelector(".nowplaying");
var hints = document.querySelectorAll(".hints");
var playButton = document.querySelector('.play');
var audioOutput = document.querySelector('#audioOutput');
var clearButton = document.querySelector('.clear');
var createSound = document.querySelector('.create');

var sounds = [];

function generateUuid () {
  return 'xxxxxxxx_xxxx_4xxx_yxxx_xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = Math.random() * 16 | 0
    var v = c === 'x' ? r : ((r & 0x3) | 0x8)
    return v.toString(16)
  })
}

function playNote(e) {
  var audio = document.querySelector(`audio[data-key="${e.keyCode}"]`);
  var key = document.querySelector(`.key[data-key="${e.keyCode}"]`);

  if (createSound.innerHTML != 'Create') {
    createSound.innerHTML = 'Create'
  }
  if (!key) return;

  var keyNote = key.getAttribute("data-note");

  sounds.push(key.getAttribute("data-sound"))

  key.classList.add("playing");
  note.innerHTML = keyNote;
  audio.currentTime = 0;
  audio.play();
}

function removeTransition(e) {
  if (e.propertyName !== "transform") return;
  this.classList.remove("playing");
}

function hintsOn(e, index) {
  e.setAttribute("style", "transition-delay:" + index * 50 + "ms");
}

hints.forEach(hintsOn);

keys.forEach(function(key) {
  key.addEventListener("transitionend", removeTransition)
});

window.addEventListener("keydown", playNote);

playButton.addEventListener("click", function(e) {
  e.preventDefault();
  audioOutput.play();
})

clearButton.addEventListener("click", function(e) {
  e.preventDefault();
  audioOutput.setAttribute("src", "")
  playButton.style.display = 'none'
  clearButton.style.display = 'none'
  createSound.innerHTML = 'Create'
})


createSound.addEventListener("click", function(e) {
  e.preventDefault();
  var outputID = generateUuid();
  e.target.innerHTML = 'Creating...'
  fetch('/sounds', {
    method: 'POST',
    body: JSON.stringify({ 'sounds':  sounds, 'id': outputID }),
    headers:{
      'Content-Type': 'application/json'
    }
  })
  .then(function(res) { return res.json() })
  .then(function(response) {
    if (response.success) {
      e.target.innerHTML = 'Done'
      playButton.style.display = null
      clearButton.style.display = null
      audioOutput.setAttribute("src", "/assets/" + outputID + ".wav")
    } else {
      e.target.innerHTML = 'Play Something'
    }
  })
  .catch(function(error) { console.log('Error:', error) });
})