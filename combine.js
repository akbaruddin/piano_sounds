const child_process = require('child_process');

const SOX_PATH = './sox';

const combine = (files, output, callback) => {
  const command = `${SOX_PATH} ${files.join(' ')} ${output} norm`;
  console.log(command)
  child_process.exec(command, (err/*, stdout, stderr*/) => {
    callback(err);
  });
}

module.exports.combine = combine;

