const express = require('express');
const router = express.Router();
const { combine } = require('../combine');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/', function(req, res, next) {
  var files = req.body.sounds.map(function(item){ return './public/sounds/' + item + '.wav' })
  console.log(files);
  combine(files, './public/assets/'+ req.body.id + '.wav', function(error){
    console.log(!error)
    if (!error) {
      res.send({ success: 1 });
    } else {
      res.send({ success: 0 });
    }
  })
  
});

module.exports = router;