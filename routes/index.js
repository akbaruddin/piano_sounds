var express = require('express');
var router = express.Router();

const PINO_KEYS = [
  { 'key': '65', 'note': 'C', 'name': 'A', 'class': '', 'sound': '040'},
  { 'key': '87', 'note': 'C#', 'name': 'W', 'class': 'sharp', 'sound': '041'},
  { 'key': '83', 'note': 'D', 'name': 'S', 'class': '', 'sound': '042'},
  { 'key': '69', 'note': 'D#', 'name': 'E', 'class': 'sharp', 'sound': '043'},
  { 'key': '68', 'note': 'E', 'name': 'D', 'class': '', 'sound': '044'},
  { 'key': '70', 'note': 'F', 'name': 'F', 'class': '', 'sound': '045'},
  { 'key': '84', 'note': 'F#', 'name': 'T', 'class': 'sharp', 'sound': '046'},
  { 'key': '71', 'note': 'G', 'name': 'G', 'class': '', 'sound': '047'},
  { 'key': '89', 'note': 'G#', 'name': 'Y', 'class': 'sharp', 'sound': '048'},
  { 'key': '72', 'note': 'A', 'name': 'H', 'class': '', 'sound': '049'},
  { 'key': '85', 'note': 'A#', 'name': 'U', 'class': 'sharp', 'sound': '050'},
  { 'key': '74', 'note': 'B', 'name': 'J', 'class': '', 'sound': '051'},
  { 'key': '75', 'note': 'C', 'name': 'K', 'class': '', 'sound': '052'},
  { 'key': '79', 'note': 'C#', 'name': 'O', 'class': 'sharp', 'sound': '053'},
  { 'key': '76', 'note': 'D', 'name': 'L', 'class': '', 'sound': '054'},
  { 'key': '80', 'note': 'D#', 'name': 'P', 'class': 'sharp', 'sound': '055'},
  { 'key': '186', 'note': 'E', 'name': ';', 'class': '', 'sound': '056'},
]

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'PIANO', pino_keys: PINO_KEYS });
});

module.exports = router;
